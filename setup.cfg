[metadata]
name = finesse
author_email = finesse-support@nikhef.nl
license = GPL
license_files = LICENSE.md
url = https://www.gwoptics.org/finesse
description = Simulation tool for modelling laser interferometers
long_description = file: README.md
long_description_content_type = text/markdown
classifiers =
    Development Status :: 3 - Alpha
    Topic :: Scientific/Engineering :: Physics
    Intended Audience :: Science/Research
    Natural Language :: English
    License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
    Operating System :: Microsoft :: Windows
    Operating System :: POSIX
    Operating System :: Unix
    Operating System :: MacOS
    Programming Language :: Python :: 3
    Programming Language :: Python :: 3.9
    Programming Language :: Python :: 3.10
    Programming Language :: Python :: 3.11
    Programming Language :: Python :: 3.12
project_urls =
    Bug Tracker = https://gitlab.com/ifosim/finesse/finesse3/issues
    Source Code = https://gitlab.com/ifosim/finesse/finesse3
    Documentation = https://finesse.ifosim.org/docs/

[options]
package_dir =
    = src
packages = find:
zip_safe = false
python_requires = >=3.9
install_requires =
    # See https://finesse.docs.ligo.org/finesse3/developer/codeguide/requirements.html
    # for more information on how to set requirements.
    # The packages here should match entries in `pyproject.toml` build-system
    # requirements (for build requirements) and `environment.yml` /
    # `environment-win.yml` (for runtime requirements).
    # Note to package maintainers: these versions may not be the absolute minimum
    # requirements; it may be possible to reduce them if they are problematic for
    # packaging - get in touch with developers.
    numpy >= 1.20, < 2.0
    scipy >= 1.4 # NB: for cython3 SciPy>=1.11.2, see !159#note_1619762810
    matplotlib >= 3.5
    networkx >= 2.4
    sly >= 0.4
    click >= 7.1
    click-default-group >= 1.2.2
    control >= 0.9
    cython >= 0.29.36
    sympy >= 1.6
    more-itertools >= 8.7
    tqdm >= 4.40
    h5py >= 3.0
    deprecated >= 1.2
    pyspellchecker >= 0.6
    quantiphy >= 2.15
    # blockdiag >= 3.0.0 # Needs 3.11 fixing on conda forge first

[options.packages.find]
where=src

[options.package_data]
finesse = finesse.ini, usr.ini.dist
finesse.plotting.style = *.mplstyle

[options.entry_points]
console_scripts =
    kat3 = finesse.__main__:cli
pygments.lexers =
    KatScript = finesse.script.highlighter:KatScriptPygmentsLexer
    KatScriptInPython = finesse.script.highlighter:KatScriptSubstringPygmentsLexer

[options.extras_require]
# Optional extras. These should not be pinned to specific versions so that we can
# continuously test the latest versions. One exception would be for when there are
# incompatibilities with new versions, in which case pin to a working version and leave
# a comment as to why.
test =
    pytest
    pytest-random-order
    pytest-benchmark
    faker
    hypothesis
    docutils
    black
    autoflake
    isort
    jupyter_sphinx
docs =
    sphinx < 6
    sphinx_rtd_theme
    sphinxcontrib-bibtex
    sphinxcontrib-katex
    sphinxcontrib-svg2pdfconverter
    sphinxcontrib-programoutput
    jupyter-sphinx
    numpydoc
    reslate
lint =
    black
    pre-commit
    tox
    flake8
    flake8-bugbear
    doc8
inplacebuild =
    # Dependencies required to be able to rebuild Finesse extensions in-place, e.g.
    # using `make`. See docs for info about why this exists:
    # https://finesse.docs.ligo.org/finesse3/developer/setting_up.html#rebuilding-extensions.
    # These requirements (and versions, if present) must match those in pyproject.toml
    # build-system requirements.
    setuptools >= 45
    setuptools_scm >= 8.0.3
    wheel
    numpy >= 1.20, < 2.0
    cython >= 0.29.36
    scipy >= 1.4 # NB: for cython3 SciPy>=1.11.2, see !159#note_1619762810
    tqdm >= 4.40
graphviz =
    pygraphviz  # Requires graphviz to be available on the system.

[flake8]  # Annoyingly, flake8 doesn't support pyproject.toml so we put this here...
# Ignored rules.
ignore =
    ### Errors.
    # Whitespace before ':'
    E203
    # Too many leading '#' for block comment
    E266
    # Module level import not at top of file
    E402
    # Line too long
    E501
    # Do not assign a lambda expression, use a def
    E731
    # ambiguous variable name
    E741
    # missing whitespace after ','
    E231
    ### Warnings.
    # line break before binary operator (soon deprecated; see https://www.flake8rules.com/rules/W503.html)
    W503

# Excluded patterns.
exclude =
    .git
    __pycache__
    src/docs/source/conf.py
    build
    dist
