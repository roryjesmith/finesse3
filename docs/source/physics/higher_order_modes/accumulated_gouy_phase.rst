.. include:: /defs.hrst
.. _accumulated_gouy_phase:

Accumulated Gouy phase
~~~~~~~~~~~~~~~~~~~~~~~~

Every electric field that propagates through free space of length :math:`d` it accumulates a phase given by

.. math::
        \psi_\text{planewave} = \exp\left(i k d\right)

To help us distinguish it from other forms of phase we will call :math:`\psi_\text{planewave}` the planewave phase.

Transverse electric fields with finite extent accumulate an additional overall phase based on their transverse geometry.
The manifestation of this geometric phase in Hermite-Gaussian modes is referred to as the Gouy phase.
It is common to associate the Gouy phase with the Gaussian q-paramter in the following way

.. math::
        \psi_{nm} = \psi_n \psi_m

where

.. math::
        \psi_n = \exp \left(i \left(n + 1/2 \right) \text{arctan}\left(\frac{\text{Real}[q]}{\text{Imag}[q]}\right) \right)

which simplifies (using Mathematica) to

.. math::
        \psi_n = \left(\frac{iq^*}{|q|}\right)^{\left(n + 1/2 \right)}

In the case of just free space propagation the two preceeding equations produce the correct phase for all HG modes.
However this is not true for propagation through a thin lens or reflecting off of a curved mirror.
In both cases encoding the Gouy phase with the q-parameter leads to a discontinuous change in phase when comparing the beam before and after the lens or mirror.
This is because Gouy phase is an accumulated effect that can't be computed from an instantaneous description of a beam such as the q-parameter.
This can be treated by introducing a convention of only accumulating Gouy phase across a space and neglecting any Gouy phase across a mirror or a lens.
Alternatively one could use the accumulated Gouy phase formulas.

.. math::
        \psi_n = \left(\frac{A + B / q^*}{|A+B/q^*|}\right)^{\left(n + 1/2 \right)}
        :label: eq_accum_gouy

where :math:`A` and :math:`B` are the ABCD parameters for a particular propagation and :math:`q` is the q-parameter taken just before the propagation.
This formula produces the correct accumulated Gouy phase for all basic components (spaces, lenses, mirrors).
It should be noted that using the formula with a single ABCD matrix representing an overall optical system with many lenses and spaces may be off by a minus sign (particularly if the system is astigmatic).
This can be avoided by only using the accumulated Gouy phase formula for simple optical components and multiplying all :math:`\psi_n`'s to get the overall accumulated Gouy phase of an optical system.

Parity transformations on reflection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mirrors have an additional level of complexity when compared to lenses since they also flip the beam left to right on reflection.
If we consider a perfectly reflective flat mirror then the reflected and incident electric field are related by the following coordinate transformation

.. math::
    \begin{aligned}
        E_\text{reflected}(x,y) &= \mathcal{P}[E_\text{incident}]\\
            &= E_\text{incident}(-x,y)
    \end{aligned}

This particular operation is known as a parity transformation :math:`\mathcal{P}`.
If the electric field is composed of Hermite-Gauss modes then the parity operator has a particularly simple form

.. math::
    \mathcal{P}[u_{nm}] =
        \begin{cases}
            u_{nm} & \text{if } n \text{ even}\\
            -u_{nm} & \text{if } n \text{ odd}
        \end{cases}

The parity operator can be implemented by directly applying the appropriate minus signs to the mode coefficients on reflection.
A more elegant solution is to use the fact that a parity transformation is representable with an ABCD matrix and to use Eq. :eq:`eq_accum_gouy` to compute the appropriate minus signs.
The relevant ABCD matrices that implement the parity operator :math:`\mathcal{P}` are

.. math::
    \begin{aligned}
        \mathbf{M}_{\mathcal{P},x} = \begin{bmatrix} -1 & 0 \\ 0 & -1 \end{bmatrix} \qquad \qquad \mathbf{M}_{\mathcal{P},y} = \begin{bmatrix} 1 & 0 \\ 0 & 1 \end{bmatrix}
    \end{aligned}

Alternatively we can consider what the modified ABCD matrices are for reflecting off of a curved mirror

.. math::
    \begin{aligned}
        \mathbf{M}_x = \begin{bmatrix} -1 & 0 \\ 2/R_{c,x} & -1 \end{bmatrix} \qquad \qquad \mathbf{M}_y = \begin{bmatrix} 1 & 0 \\ -2/R_{c,y} & 1 \end{bmatrix}.
    \end{aligned}

One may wonder if encoding the parity operator with an ABCD matrix could somehow introduce changes to the standard q-parameter propagation formulas.
:math:`\mathbf{M}_{\mathcal{P},y}` doesn't change anything because it's the identity matrix.
For :math:`\mathbf{M}_{\mathcal{P},x}` one can use the standard q-parameter propagation formula

.. math::
    \begin{aligned}
        q_2 &= \frac{A q_1 + B}{C q_1 + D}\\
            &= \frac{-1 q_1}{-1}\\
            &= q_1
    \end{aligned}

and so :math:`\mathbf{M}_{\mathcal{P},x}` has no effect on q-parameter propagation.
Higher order scattering matrices also remain uneffected since encoding the parity transformation with ABCD matrices has no effect on q-parameters.

So now if we calculate the accumulated Gouy phase using :eq:`eq_accum_gouy` we get the following

.. math::
    \begin{aligned}
        \psi_n &= \left(\frac{A + B / q^*}{|A+B/q^*|}\right)^{\left(n + 1/2 \right)}\\
        &= \left(\frac{-1}{|-1|}\right)^{\left(n + 1/2 \right)}\\
        &= \left(-1\right)^{\left(n + 1/2 \right)}\\
        &= (-1)^{1/2} (-1)^{n}\\
        &= i (-1)^{n}
    \end{aligned}

which produces a minus sign difference between odd and even modes as desired.
