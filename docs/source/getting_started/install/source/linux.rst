.. include:: ../../../defs.hrst
.. _linux-source-installation:

Linux
=====

.. todo:: add other linux distros

Debian based systems
--------------------

Debian-based systems need the following packages:

.. code-block:: console

    $ apt install make gcc python3 python3-dev python3-pip git libsuitesparse-dev

Once the relevant system packages are installed simply clone the repository and build
|Finesse| as described below. By default this will install the latest stable version.
See below for details on how to change to different versions such as the most current
development release.

.. code-block:: console

    $ git clone -b master https://gitlab.com/ifosim/finesse/finesse3.git
    $ cd finesse3
    $ make install-pep517

This will build and install |Finesse| into your local environment's library path.
Installing |Finesse| can take some time as it needs to compile some extensions before it
can run. You should see no errors and the version number installed should be printed.
Once you've run this command, you can quickly test if it has worked by running:

.. code-block:: console

    $ kat3 --version

If you see the |Finesse| version printed, everything should be configured correctly.

Rebuilding |Finesse|
--------------------

Once you have installed |Finesse| from source you can easily get updates and rebuild it
by executing the following commands from the |Finesse| repository directory:

.. code-block:: console

    $ git pull
    $ make clean
    $ make install-pep517

This should rebuild |Finesse| and can take some time. For small changes to the source
you can omit ``make clean``.

Occasionally changes to the build tooling will require a complete reset of the local
clone of the |Finesse| repository. To do this, the command ``make realclean`` can be
used above instead of ``make clean``.

Changing |Finesse| versions after installation
----------------------------------------------

Switching versions in |Finesse| involves checking out a different git branch. The
process is similar to above when rebuilding, except you use ``git checkout`` beforehand.
It is recommended to use ``make clean`` when switching branches or commits.

.. code-block:: console

    $ cd finesse3
    $ git checkout [ref]
    $ git pull
    $ make clean
    $ make install-pep517

You can replace ``[ref]`` with one of:

``master``
    The latest stable release.

``develop``
    The latest development release (not necesarily stable or runnable).

e.g. ``3.0a1``
    A particular tag.
