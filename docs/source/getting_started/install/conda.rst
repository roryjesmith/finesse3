.. include:: ../../defs.hrst
.. _installation-conda:

From Conda
----------

`Conda <https://conda.io/>`__ is a cross-platform, open source package manager. It
allows users to switch between software environments on their computers, which enables
software with different, possibly conflicting requirements to be run independently of
each other.

|Finesse| is available as a `conda-forge <https://anaconda.org/conda-forge/finesse>`__ package.

Installation instructions can be found in the following section. It is not required, but
recommended, to first create a new environment in which to install |Finesse|. 

Linux / macOS
~~~~~~~~~~~~~

(Optional) Create and activate a new environment for |Finesse|:

.. code-block:: console

    $ conda create -n finesse
    $ conda activate finesse

Install the |Finesse| package from conda-forge:

.. code-block:: console

    (finesse) $ conda install -c conda-forge finesse

Windows
~~~~~~~

(Optional) Create and activate a new environment for |Finesse|:

.. code-block:: doscon

    C:\> conda create -n finesse
    C:\> conda activate finesse

Install the |Finesse| package from conda-forge:

.. code-block:: doscon

    (finesse) C:\> conda install -c conda-forge finesse


