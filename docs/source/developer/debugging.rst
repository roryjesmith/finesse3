.. include:: ../defs.hrst

.. _debugging:

Debugging Finesse
=================

Errors originating from C code compiled via Cython often do not yield useful error
messages. Instead you may encounter segmentation faults without any additional
information to point to the error origin. Setting up a debugger for cython code is quite complicated, so there is a dockerfile included in the repo that sets up a debugging environment. See `the readme in the docker folder <https://gitlab.com/ifosim/finesse/finesse3/-/blob/develop/docker/README.md?ref_type=heads>`_ for instructions.
