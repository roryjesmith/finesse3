.. include:: /defs.hrst

API Documentation
=================

This reference manual details the modules, classes and functions included in |Finesse|,
describing what they are, what they do and (where relevant) mathematical descriptions of
how they work. More in-depth coverage of the physics behind the code can be found in
:ref:`physics`.

For learning how to get started with using |Finesse|, see also :ref:`getting_started`.

.. autosummary::
    :toctree: .

    finesse
